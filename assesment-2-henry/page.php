<?php get_header(); ?>

<?php get_sidebar(); ?>

<div class="content">
	<?php
	while ( have_posts() ) : the_post();

		the_content();

	endwhile;
	?>
</div>

<?php get_footer(); ?>